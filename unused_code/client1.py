import base64
import requests

url = 'http://127.0.0.1:8000/classify_pose'
with open("/home/ajaytiwari/Documents/poseoff/data/exp.png", "rb") as image_file:
	encoded_string = base64.b64encode(image_file.read())

payload = {"pose_name": "photo.png", "filedata": encoded_string}
print(encoded_string)
resp = requests.post(url=url, data=payload)